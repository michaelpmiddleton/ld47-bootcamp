﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallManager : MonoBehaviour {
    public static GameObject actualBall;

    [SerializeField]
    private GameObject ballPrefab;


    // Update is called once per frame
    void Update() {
        if (actualBall == null) {
            actualBall = Instantiate(ballPrefab, transform.parent);
        }
    }
}
