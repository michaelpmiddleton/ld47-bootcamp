﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    [SerializeField]
    float moveSpeed = 1.0f;
    [SerializeField]
    float rotationSpeed = 1.0f;
    [SerializeField]
    private Rigidbody rigidbody;
    [SerializeField]
    private Camera cam;
    private Vector3 movement = new Vector3(1, 0, 1);
    private Vector3 rotation = new Vector3(0, 0, 0);

    void Start() {
        rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate() {
        movement.x = Input.GetAxis("Vertical");
        movement.z = Input.GetAxis("Horizontal");
        rotation.y = Input.GetAxis("Mouse X");

        if (movement.x + movement.z != 0f) {
            rigidbody.AddForce(transform.TransformDirection(movement * moveSpeed));
        }
        
        if (rotation.y != 0f) transform.Rotate(rotationSpeed * rotation);
    }
}
