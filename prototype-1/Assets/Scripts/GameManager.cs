﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour {
    private static GameManager gm;
    private int lScore, rScore;
    private TextMeshProUGUI scoreText;

    // Start is called before the first frame update
    void Start() {
        if (gm == null) {
            gm = this;
        }
        gm.lScore = gm.rScore = 0;
        gm.scoreText = GetComponentInChildren<TextMeshProUGUI>();
        gm.scoreText.SetText($"LEFT: {gm.lScore}\nRIGHT: {gm.rScore}");
    }

    public static void IncrementScore(int side) {
        if (side > 0) gm.rScore++;
        else gm.lScore++;
        gm.scoreText.SetText($"LEFT: {gm.lScore}\nRIGHT: {gm.rScore}");
    }
}
