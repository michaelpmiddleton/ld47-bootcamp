﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalScript : MonoBehaviour {
    private BoxCollider goal;
    [SerializeField]
    private int side;


    // Start is called before the first frame update
    void Start() {
        goal = GetComponent<BoxCollider>();
    }

    private void OnTriggerEnter(Collider other) {
        Destroy(BallManager.actualBall.gameObject);
        GameManager.IncrementScore(side);
        Debug.Log("GOAL!!! (" + gameObject.name + ")");
    }

}
