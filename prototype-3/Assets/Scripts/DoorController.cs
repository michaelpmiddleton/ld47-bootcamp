﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    //Position Vectors
    public Vector3 openPosition;
    public Vector3 closedPosition;

    //Door Speed
    public float speed;
    public float delay;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("StartCycle");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator StartCycle(){
        yield return new WaitForSeconds(delay);
        StartCoroutine("Close");
    }

    IEnumerator Open(){
        yield return new WaitForSeconds(speed);
        gameObject.transform.position = openPosition;
        StartCoroutine("Close");
    }

    IEnumerator Close(){
        yield return new WaitForSeconds(speed);
        gameObject.transform.position = closedPosition;
        StartCoroutine("Open");
    }
}
