﻿using UnityEngine;
using System.Drawing;
using TwitchLib.Unity;
using TwitchLib.Client.Models;

public class Twitch : MonoBehaviour {
    private Client chatClient;
    private string channel = "saphirako";

    // Unity Functions  //////////////////////////////////////////
    void Start() {
        // Initialize Client using secrets        
        chatClient = new Client();
        chatClient.Initialize(new ConnectionCredentials("saphirakobot", Secrets.bot_access_token), channel);

        // Connect events to functions in the Twitch class
        chatClient.OnConnected += OnConnected;
        chatClient.OnChatCommandReceived += OnChatCommandReceived;

        // Connect to chat
        chatClient.Connect();
    }


    // Events   //////////////////////////////////////////////////    
    void OnConnected(object sender, TwitchLib.Client.Events.OnConnectedArgs e) {
        NotifyOpenLobby();
    }
    void OnChatCommandReceived(object sender, TwitchLib.Client.Events.OnChatCommandReceivedArgs e) {
        switch (e.Command.CommandText) {
            case "join":
                if (GameManager.CreatePlayer(e.Command.ChatMessage.DisplayName, e.Command.ChatMessage.ColorHex))
                    Debug.Log($"Twitch:\tAdded user '{e.Command.ChatMessage.DisplayName}' to the current game.");
                else {
                    if (GameManager.HasOpenLobby())
                        chatClient.SendMessage(channel, $"Sorry {e.Command.ChatMessage.DisplayName}, you can only join once.");
                    else
                        chatClient.SendMessage(channel, $"The lobby is no longer accepting new participants.");
                }
                break;
            default:
                Debug.Log($"Twitch: Unknown command '{e.Command.CommandText}'");
                break;
        }
    }


    // Other functions
    public void NotifyWinner(string username) {
        Debug.Log($"Twitch:\t User '{username}' has won!");
        chatClient.SendMessage(channel, $"Congratulations {username}! You won the race. PogChamp");
    }
    public void NotifyOpenLobby() {
        chatClient.SendMessage(channel, "Hey everyone, the lobby for Prototype-3 is now open!");
        Debug.Log("Twitch:\tLobby notification delivered.");
    }
}
