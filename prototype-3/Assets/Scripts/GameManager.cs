﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GameManager : MonoBehaviour {
    private static GameManager gm;
    [SerializeField]
    Collider finalDestination;
    [SerializeField]
    private Canvas canvas;
    [SerializeField]
    private Vector3 spawnBound1 = new Vector3(0, 0, 0);
    [SerializeField]
    private Vector3 spawnBound2 = new Vector3(0, 0, 0);
    [SerializeField]
    private AgentSript playerPrefab;
    [SerializeField]
    private Color defaultColor;
    [SerializeField]
    private float playerSpeed;
    [SerializeField]
    private Camera[] cameras;
    private List<string> playersInGame;
    private Twitch twitch;
    private bool accepting_new_players;


    void Awake() {
        gm = this;                      // Sets the static GameManager to the current instance
        gm.playersInGame = new List<string>();
        DontDestroyOnLoad(gameObject);  // Do not delete the GameManager when loading other scenes
    }

    void Start() {
        gm.twitch = GetComponent<Twitch>();
        gm.accepting_new_players = true;
    }
    void Update() {
        if (Input.GetKeyUp(KeyCode.Space)) {
            if (!gm.playersInGame.Contains("saphirakobot"))
                CreatePlayer("saphirakobot", null);

            else
                Destroy(GameObject.Find("player_saphirakobot"));
        } else if (Input.GetKeyUp(KeyCode.Return)) {
            gm.accepting_new_players = false;
            foreach (NavMeshAgent player in canvas.GetComponentsInChildren<NavMeshAgent>()) {
                player.speed = gm.playerSpeed;
            }
        } else if (Input.GetKeyUp(KeyCode.Delete)) {
            for (int c = gm.canvas.transform.childCount-1; c >= 0; c--)
                Destroy(gm.canvas.transform.GetChild(c).gameObject);
            gm.playersInGame.Clear();
            gm.ActivateCamera(0);
            gm.finalDestination.enabled = true;
            gm.twitch.NotifyOpenLobby();
        } else if (Input.GetKeyUp(KeyCode.Alpha1)) gm.ActivateCamera(0);
        else if (Input.GetKeyUp(KeyCode.Alpha2)) gm.ActivateCamera(1);
        else if (Input.GetKeyUp(KeyCode.Alpha3)) gm.ActivateCamera(2);
        else if (Input.GetKeyUp(KeyCode.Escape)) {
            Application.Quit();
        }
    }
    public static bool CreatePlayer(string name, string color) {
        // Return failure
        if (!gm.accepting_new_players)
            return false;
        else if (gm.playersInGame.Contains(name))
            return false;


        // Get a position at random to spawn the player
        float xPos = Random.Range(gm.spawnBound1.x, gm.spawnBound2.x);
        float zPos = Random.Range(gm.spawnBound1.z, gm.spawnBound2.z);

        // Assign chat color if possible, else, set it to 
        Color newColor;
        if (!ColorUtility.TryParseHtmlString(color, out newColor))
            newColor = gm.defaultColor;

        AgentSript newPlayer = Instantiate(gm.playerPrefab, new Vector3(xPos, 0, zPos), gm.transform.rotation, gm.canvas.transform);
        newPlayer.Initialize(newColor, name);
        newPlayer.gameObject.name = $"player_{name}";

        // Add player to the list of current players
        gm.playersInGame.Add(name);

        // Return success
        return true;
    }

    //Set Winner
    public static void SetWinner(AgentSript other) {
        gm.twitch.NotifyWinner(other.playerName);
    }

    public static Vector3 GetFinalDestination() {
        return gm.finalDestination.transform.position;
    }

    public static bool HasOpenLobby() {
        return gm.accepting_new_players;
    }
    private void ActivateCamera(int index) {
        gm.cameras[index].enabled = true;
        for (int c = 0; c < gm.cameras.Length; c++) {
            if (c == index)
                continue;
            gm.cameras[c].enabled = false;
        }
    }
}
