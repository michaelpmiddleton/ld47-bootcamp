﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.AI;

public class AgentSript : MonoBehaviour
{
    public string playerName;
    private NavMeshAgent agent;
    private Material material;
    private TextMeshProUGUI label;

    void Awake() {
        material = GetComponent<MeshRenderer>().material;
        label = GetComponent<TextMeshProUGUI>();
        agent = GetComponent<NavMeshAgent>();
    }

    void Start() {
        agent.destination = GameManager.GetFinalDestination();    
    }

    void Update() {
        if (Camera.current != null)
            label.transform.LookAt(2 * transform.position - Camera.current.transform.position);
    }

    public void Initialize(Color color, string name) {
        material.color = color;
        playerName = name;
        label.SetText(name);
    }

    private void OnTriggerEnter(Collider other){
        if (other.gameObject.tag == "Finish"){
            other.enabled = false;
            GameManager.SetWinner(this);
        }
    }
}
