﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallScript : MonoBehaviour {
    public Transform tpLocation;
    [SerializeField]
    private ParticleSystem tpFX;


    void Start() {
        tpLocation = transform.GetChild(0);
    }

    public void Teleport() {
        Instantiate(tpFX, tpLocation.position, tpLocation.rotation);
    }
}
