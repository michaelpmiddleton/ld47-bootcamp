﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour {

    [SerializeField]
    float moveSpeed = 1.0f;
    [SerializeField]
    float rotationSpeed = 1.0f;
    private Rigidbody rigidbody;
    private Vector3 movement = new Vector3(1, 0, 1);
    private Vector3 rotation = new Vector3(0, 0, 0);
    [SerializeField]
    private ParticleSystem flames;
    [SerializeField]
    private GameObject projectileInstance;
    [SerializeField]
    private float projectileRate;
    private float cartridge = 0;
    private Transform projectileSpawnLocation;
    public static Collider WorldLimiter;
    [SerializeField]
    private float projectileSpeed;
    [SerializeField]
    private Vector3 projectileForce;



    void Start() {
        rigidbody = GetComponent<Rigidbody>();
        Cursor.lockState = CursorLockMode.Locked;
        projectileSpawnLocation = transform.GetChild(0);
        WorldLimiter = GameObject.FindWithTag("worldlimiter").GetComponent<Collider>();
    }

    void Update() {
        // Unlock the mouse from being trapped in 
        if (Input.GetKeyUp(KeyCode.Escape))
            Cursor.lockState = Cursor.lockState == CursorLockMode.None ? CursorLockMode.Locked : CursorLockMode.None;

        if (Input.GetAxis("Fire1") != 0) {
            if (cartridge <= 0) {
                ProjectileScript newProjectile = Instantiate(projectileInstance, projectileSpawnLocation.position, transform.rotation).GetComponent<ProjectileScript>();
                newProjectile.rb.AddForce(
                    projectileSpawnLocation.TransformDirection(projectileForce * projectileSpeed)
                );
                cartridge = projectileRate;
            }
        }
        cartridge -= Time.deltaTime;
    }


    void FixedUpdate() {
        movement.x = Input.GetAxis("Vertical");
        movement.z = Input.GetAxis("Horizontal");
        rotation.z = Input.GetAxis("Mouse Y");
        rotation.y = Input.GetAxis("Mouse X");

        if (movement.x + movement.z != 0f) {
            rigidbody.AddForce(transform.TransformDirection(movement * moveSpeed));
        }
        
        if (rotation.y + rotation.z != 0f) transform.Rotate(rotationSpeed * rotation);
    }

    public void Teleport(WallScript wall) {
        // Get where we were for the flames
        Vector3 oldPos = transform.position;
        
        // PLay the particle and move
        wall.Teleport();
        transform.SetPositionAndRotation(wall.tpLocation.position, wall.tpLocation.rotation);

        // Add BTTF flames
        Instantiate(flames, oldPos, transform.rotation);
    }
}
