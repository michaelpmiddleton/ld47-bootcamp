﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
    private static GameManager gm;
    public static PlayerScript Player {
        get {return gm.player;}
        set {gm.player = value;}
    }
    [SerializeField]
    private PlayerScript player;

    void Start() {
        gm = this;
    }
}
