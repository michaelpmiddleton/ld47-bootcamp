﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileScript : MonoBehaviour {
    public Rigidbody rb;

    void Awake() {
        rb = GetComponent<Rigidbody>();    
    }

    void OnCollisionEnter(Collision other) {
        if (other.gameObject.tag == "wall") {
            GameManager.Player.Teleport(other.gameObject.GetComponent<WallScript>());
            Destroy(gameObject);
        }
    }

    void OnTriggerExit(Collider other) {
        if (other == PlayerScript.WorldLimiter) {
            Destroy(gameObject);
        }    
    }
}
