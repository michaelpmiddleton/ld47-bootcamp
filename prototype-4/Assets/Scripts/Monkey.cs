﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monkey : MonoBehaviour {
    public enum Actions {NONE=3, IDLE=4, WALK=1, RUN=0, SPRINT=2, JUMP=5};
    public Transform Goal;
    public Actions Action;
    public Animator AnimationController;
    public float CoolDown;
    public float Speed;
    public bool CanMove;
    private float timer;

    void Awake() {
        AnimationController = GetComponent<Animator>();
    }

    void Start() {
        timer = CoolDown;
    }

    void Update() {
        if (timer < 0) {
            PerformAction();
            timer = CoolDown;
        }
        timer -= Time.deltaTime;

        // Monkey 1:
        if (CanMove && Goal != null) {
            transform.position = Vector3.Lerp(transform.position, Goal.position, (Time.deltaTime / Speed));
            transform.LookAt(Goal);
        }
    }

    void PerformAction() {
        switch (Action) {
            case Actions.IDLE:
                AnimationController.SetTrigger("CycleIdle");
                break;
            case Actions.WALK:
                CanMove = true;
                AnimationController.SetBool("Walk", true);
                break;
            case Actions.RUN:
                AnimationController.SetBool("Run", !AnimationController.GetBool("Run"));
                break;
            case Actions.SPRINT:
                AnimationController.SetBool("Sprint", !AnimationController.GetBool("Sprint"));
                break;
            default:
                break;
        }
    }
}
