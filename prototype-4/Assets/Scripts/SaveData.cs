using System.Collections.Generic;

[System.Serializable]
public class SaveData {
    public int count;
    public Queue<int> target = new Queue<int>();
    public Queue<int> action = new Queue<int>();
}