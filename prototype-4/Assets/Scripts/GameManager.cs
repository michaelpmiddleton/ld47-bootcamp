﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour {
    public Monkey[] Monkeys;
    public Transform TargetsForMonkey1;
    public TMP_Dropdown Monkey1, Monkey2;
    private string savePath;

    // Unity functions ////////////////////////////////////////////
    void Awake() {
        savePath = $"{Application.persistentDataPath}/prototype-4.save";
        LoadGame();
    }


    // UI Interactions  ///////////////////////////////////////////
    public void SetMonkey1Action(int index) {
        Monkeys[0].Goal = TargetsForMonkey1.GetChild(index);
    }
    public void SetMonkey2Action(int index) {
        Monkeys[1].Action = (Monkey.Actions)index;
    }
    public void SaveGame() {
        // Put data in the serializable object
        SaveData save = new SaveData();
        save.count = Monkeys.Length;
        foreach (Monkey monkeh in Monkeys) {
            save.target.Enqueue(monkeh.Goal == null ? -1 : monkeh.Goal.GetSiblingIndex());
            save.action.Enqueue((int)monkeh.Action);
        }

        // Save the serializable object to a system file
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(savePath, FileMode.Create);
        bf.Serialize(file, save);
        file.Close();
        Debug.Log("Prototype saved!");
    }


    // Other functions  ////////////////////////////////////////////
    void LoadGame() {
        // Check for file and load if possible
        if (File.Exists(savePath)) {
            Debug.Log("Save file detected! Loading in user preferences.");

            // Load data from filestore
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(savePath, FileMode.Open);
            SaveData savedData = (SaveData)bf.Deserialize(file);

            // Plug in parameters as follows
            int index;
            for (int c = 0; c < savedData.count; c++) {
                index = savedData.target.Dequeue();
                Monkeys[c].Action = (Monkey.Actions)savedData.action.Dequeue();
                Monkeys[c].Goal = index == -1 ? null : TargetsForMonkey1.GetChild(index);
            }

            // Update the dropdowns to be set properly
            Monkey1.SetValueWithoutNotify(Monkeys[0].Goal.GetSiblingIndex());
            Monkey2.SetValueWithoutNotify((int)Monkeys[1].Action);
        }
        else
            Debug.Log("No save file detected. Running with defaults.");
    }
}
